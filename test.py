import unittest

class testBusinessLogic(unittest.TestCase):

	phaseID_main = 0
	phaseID_sub = 0

	def incrementPhase_main(self):
		self.phaseID_main += 1
		self.phaseID_sub = 0

	def incrementPhase_sub(self):
		if self.phaseID_main == 0:
			self.phaseID_main = 1
		self.phaseID_sub += 1

	### test cases ###

	def testMainPhaseAtStart(self):
		self.assertEqual(0, self.phaseID_main)

	def testMainPhaseAfterIncrement(self):
		self.incrementPhase_main()
		self.assertEqual(1, self.phaseID_main)

	def testSubPhaseAtStart(self):
		self.assertEqual(0, self.phaseID_sub)

	def testIncrementSubPhase(self):
		self.incrementPhase_sub()
		self.assertEqual(1, self.phaseID_sub)

	def testIncrementSubAndMain(self):
		self.incrementPhase_sub()
		self.assertEqual(1, self.phaseID_main)
		self.assertEqual(1, self.phaseID_sub)

	def testIncrementTwice(self):
		self.incrementPhase_main()
		self.incrementPhase_main()
		self.incrementPhase_sub()
		self.incrementPhase_sub()
		self.assertEqual(2, self.phaseID_main)
		self.assertEqual(2, self.phaseID_sub)

	def testResetSubPhasesAfterMainInc(self):
		self.incrementPhase_main()
		self.incrementPhase_sub()
		self.incrementPhase_main()
		self.assertEqual(2, self.phaseID_main)
		self.assertEqual(0, self.phaseID_sub)


if __name__ == '__main__':
    unittest.main()
